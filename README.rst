======
Vault
======

.. image:: https://travis-ci.org/saltstack-formulas/vault-formula.svg?branch=master

Formulas for working with `Vault <http://www.vaultproject.io>`_

Available states
================

.. contents::
    :local:

``vault``
----------

Installs and configures the Vault service.


``vault.install``
-----------------

Downloads and installs the Vault binary file.

``vault.config``
----------------

Provision the Vault configuration files and sources.

``vault.service``
-----------------

Adds the Vault service startup configuration or script to an operating system.

To start the service during Salt run and enable it at boot time, you need to set the following Pillar:

.. code:: yaml

    vault:
      service: true


Testing
=======

Testing is done with `Test Kitchen <http://kitchen.ci/>`_
for machine setup and `inspec <https://github.com/chef/inspec/>`_
for integration tests.

Requirements
------------

* Ruby
* Docker

::

    gem install bundler
    bundle install
    bundle exec kitchen test all
