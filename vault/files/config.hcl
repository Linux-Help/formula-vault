listener "{{ config.listen_protocol }}" {
  address = "{{ config.listen_address }}:{{ config.listen_port }}"
  tls_disable = {{ config.tls_disable }}
{%- if self_signed_cert.enabled %}
  tls_cert_file = "/etc/vault/{{ self_signed_cert.hostname }}.pem"
  tls_key_file = "/etc/vault/{{ self_signed_cert.hostname }}-nopass.key"
{% else -%}
{%- if config.tls_cert_file %}
  tls_cert_file = "{{ config.tls_cert_file }}"
{%- endif -%}
{%- if config.tls_key_file %}
  tls_key_file = "{{ config.tls_key_file }}"
{% endif -%}
{% endif -%}
}

{%- if config.backend and config.backend.type == "s3" %}
backend "s3" {
  bucket = "{{ config.backend.bucket }}"
}
{% endif -%}

{%- if config.storage and config.storage.type == "consul" %}
storage "consul" {
  address = "{{ config.storage.address }}"
  path = "{{ config.storage.path }}"
}
{%- else -%}
storage "file" {
  path = "{{ config.data_dir }}"
}
{%- endif %}

default_lease_ttl="{{ config.default_lease_ttl }}"
max_lease_ttl="{{ config.max_lease_ttl }}"
