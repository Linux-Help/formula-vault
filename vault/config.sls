{%- from slspath + '/map.jinja' import vault with context -%}

vault-config:
  file.managed:
    - name: /etc/vault.d/config.hcl
    - source: salt://vault/files/config.hcl
    - template: jinja
    - context:
        self_signed_cert: {{ vault.self_signed_cert }}
        config: {{ vault.config }}
    - user: {{ vault.user }}
    - group: {{ vault.group }}
    - mode: 0640
    - require:
      - user: vault-user
    {%- if vault.service %}
    - watch_in:
      - service: vault
    {%- endif %}
