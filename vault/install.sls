{%- from slspath + '/map.jinja' import vault with context -%}

vault-dep-unzp:
  pkg.installed:
    - name: unzip

vault-bin-dir:
  file.directory:
    - name: /usr/local/bin
    - makedirs: True

# Create vault user
vault-group:
  group.present:
    - name: {{ vault.group }}

vault-user:
  user.present:
    - name: {{ vault.user }}
    - groups:
      - {{ vault.group }}
    - home: {{ salt['user.info'](vault.user)['home']|default(vault.config.data_dir) }}
    - createhome: False
    - system: True
    - require:
      - group: vault-group

# Create directories
vault-config-dir:
  file.directory:
    - name: /etc/vault.d
    - user: {{ vault.user }}
    - group: {{ vault.group }}
    - mode: 0750

vault-data-dir:
  file.directory:
    - name: {{ vault.config.data_dir }}
    - makedirs: True
    - user: {{ vault.user }}
    - group: {{ vault.group }}
    - mode: 0750

# Install agent
vault-download:
  file.managed:
    - name: /tmp/vault_{{ vault.version }}_linux_{{ vault.arch }}.zip
    - source: https://{{ vault.download_host }}/vault/{{ vault.version }}/vault_{{ vault.version }}_linux_{{ vault.arch }}.zip
    - source_hash: https://releases.hashicorp.com/vault/{{ vault.version }}/vault_{{ vault.version }}_SHA256SUMS
    - unless: test -f /usr/local/bin/vault-{{ vault.version }}

vault-extract:
  cmd.wait:
    - name: unzip /tmp/vault_{{ vault.version }}_linux_{{ vault.arch }}.zip -d /tmp
    - watch:
      - file: vault-download

vault-install:
  file.rename:
    - name: /usr/local/bin/vault-{{ vault.version }}
    - source: /tmp/vault
    - require:
      - file: /usr/local/bin
    - watch:
      - cmd: vault-extract

vault-clean:
  file.absent:
    - name: /tmp/vault_{{ vault.version }}_linux_{{ vault.arch }}.zip
    - watch:
      - file: vault-install

vault-link:
  file.symlink:
    - target: vault-{{ vault.version }}
    - name: /usr/local/bin/vault
    - watch:
      - file: vault-install

vault-set-cap-mlock:
  cmd.run:
    - name: "setcap cap_ipc_lock=+ep /usr/local/bin/vault-{{ vault.version }}"
    - onchanges:
      - file: vault-install
