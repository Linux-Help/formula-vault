{%- from slspath + '/map.jinja' import vault with context -%}

{%- if vault.self_signed_cert.enabled %}
self-cert-gen-script:
  file.managed:
    - name: /usr/local/bin/self-cert-gen.sh
    - source: salt://vault/files/cert-gen.sh.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644

generate-self-signed-SSL-certs:
  cmd.run:
    - name: bash /usr/local/bin/cert-gen.sh {{ vault.self_signed_cert.hostname }} {{ vault.self_signed_cert.password }}
    - cwd: /etc/vault
    - require:
      - file: self-cert-gen-script
{% endif -%}

vault-init-env:
  file.managed:
    {%- if grains['os_family'] == 'Debian' %}
    - name: /etc/default/vault
    {%- else %}
    - name: /etc/sysconfig/vault
    - makedirs: True
    {%- endif %}
    - user: root
    - group: root
    - mode: 0644
    - contents:
      - VAULT_USER={{ vault.user }}
      - VAULT_GROUP={{ vault.group }}

vault-init-file:
  file.managed:
    {%- if salt['test.provider']('service') == 'systemd' %}
    - source: salt://{{ slspath }}/files/vault.service
    - name: /etc/systemd/system/vault.service
    - template: jinja
    - context:
        user: {{ vault.user }}
        group: {{ vault.group }}
        config: {{ vault.config }}
    - mode: 0644
    {%- elif salt['test.provider']('service') == 'upstart' %}
    - source: salt://{{ slspath }}/files/vault.upstart
    - name: /etc/init/vault.conf
    - mode: 0644
    {%- else %}
    - source: salt://{{ slspath }}/files/vault.sysvinit
    - name: /etc/init.d/vault
    - mode: 0755
    {%- endif %}

{%- if vault.service %}

vault-service:
  service.running:
    - name: vault
    - enable: True
    - watch:
      - file: vault-init-env
      - file: vault-init-file

{%- endif %}
